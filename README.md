# GraniteFone

This documentation will describe how-to make a GraniteFone and the hardware and software it consists of.

The origin of the name comes from the love of the Granite State of New Hampshire. Since there is already a product with [the name of GranitePhone](https://gadgets.ndtv.com/sikur-granitephone-3025), which is what I initially wanted to use but didn't want Indian attorneys sueing me for infringement claims, so I came up with the "GraniteFone" instead. For those who do not know, New Hampshire is the "Live Free or Die" state.

If you're deciding whether you might want a GraniteFone, please [read this recent article](https://arstechnica.com/gadgets/2021/06/even-creepier-covid-tracking-google-silently-pushed-app-to-users-phones/) about how the State of Massachusetts was (and still is) force-pushing a phone application to all iPhones and non-modified (or stock) Google phones without knowledge or consent of the phone owner. This can even happen to you if you just travel thru the state of Massachusetts. **This will never happen to a GraniteFone user, as no Google Play Services are installed.**

For anyone who has been to Massachusetts recently, you may want to read [this article](
https://web.archive.org/web/20230601044050/https://i2p.rocks/blog/the-ma-local-government-just-installed-spyware-on-your-device.html) explaining how this application is government-funded persistent Malware and how you can check if your Android phone has been "infected".

## Let's get started

### 0 - To build your own or to buy one instead

* This documentation will enable you to put together your own GraniteFone. 
* I truly believe in DIY, and am always encouraging people to "make" things, so this documentation includes everything to enable anyone in the world to make a GraniteFone.

I believe we are at a time when some people might seriously consider protecting themselves from the "clutches" of big ol' [Apple](https://www.nasdaq.com/market-activity/stocks/aapl) or Google (known as the [G00g](https://www.nasdaq.com/market-activity/stocks/goog)).

### 1 - Hardware and Operating System (OS)

#### Choice of Operating System (OS)

If you want the most secure Android phone possible, then you will want to run [GrapheneOS](https://grapheneos.org) on that phone. You can can [read more](https://grapheneos.org/features) if you would like, but below is a summary of why it is the best choice:

* Private and Secure version of Android
* Does not contain any tracking, unlike the stock OS on most devices
* Does not include any of the g00g's play services or store
* Security Updates are automatically downloaded and can be applied quickly with a reboot, unlike other versions of Android
* Easy-to-use built-in firewall to let you block an application's data/WiFi connectivity
* The [GrapheneOS features and optimizations](https://grapheneos.org/features) are very impressive
* It does not run applications as root for [these reasons](https://madaidans-insecurities.github.io/android.html#rooting)

#### Choice of Hardware

The GraniteFone uses Pixel phone hardware to have substantial hardware-based support for enhancing the security of the phone's disk encryption implementation [Source](https://grapheneos.org/faq#encryption), as you want the best full device phone encryption possible and that only comes from the hardware chip in this phone. Other phones only have software encryption, which is weaker.

The GraniteFone runs GrapheneOS and [that project suggests these phones](https://grapheneos.org/faq#recommended-devices) for the best long term support. Remember to check the [unsupported devices list here](https://grapheneos.org/faq#which-legacy-devices) before buying a Pixel model, as the legacy devices have stopped receiving security updates in GrapheneOS.

### 2 - Install GrapheneOS
[Follow these instructions](https://grapheneos.org/install/) from the GrapheneOS devs to install it on your chosen Pixel phone.

* If you not into the command line, then I recommend you use the [web-based installer](https://grapheneos.org/install/web), which is recommended for most users.
* I'm a command line person, so I chose that option instead. Only suggested for technical people.

### 3 - Install those Apps

First off install or open the [F-Droid store](https://f-droid.org) to begin adding other apps.

The GraniteFone model is to only have applications that use the least amount of permissions.

These are all installable via the [F-Droid store](https://f-droid.org).

| App name            | What it does                             |
|---------------------|:----------------------------------------:|
| Aegis Authenticator | Used for 2-Factor Authentication (if needed) | 
| antennaPod          | A free and secure podcasting application | 
| Element             | The [Matrix protocol phone app](https://en.wikipedia.org/wiki/Matrix_(protocol)) for the next generation of secure E2EE decentralized messaging | 
| EteSync             | Secure, private and end-to-end encrypted calendar, contacts and tasks sync |
| EteSync Notes       | Secure, end-to-end encrypted, and privacy respecting note taking. |
| InviZible Pro       | An app to use TOR, I2P, or dnscrypt-proxy without needing root access | 
| Jitsi Meet          | Instant video conferences efficiently adapting to your scale because we all need to start looking at this great positive alternative to the un-ethical company Zoom |
| NewPipe             | An awesome safer YouTube app which (1) lets you download the video/music onto your phone from the app (2) can "subscribe" to your favorite streamers without having an account... (3) can stream other services, like PeerTube and SoundCloud | 
| Ning                 | Simple and fast local network scanner. Similar to Fing. |
| OpenVPN without root | Use this application for your OpenVPN needs without needing root access | 
| Scrambled Exif       | Remove the metadata from your pictures before sharing them |
| SecScanQR  | The QR code scanner/generator that cares about your privacy | 
| Fossify SMS Messenger| Open-source and Ad-free SMS/MMS messaging app | 
| Fossify Calendar     | Securely plan, schedule, and set reminders with our private calendar app | 
| SecondsClock         | Simple clock with seconds as widget or fullscreen | 
| Simple Flashlight    | The name says it all              | 
| Fossify Gallery      | A great gallery application to replace the built-in gallery app. No Ads, Open-source, Private. No strings attached. |
| VLC                  | The swiss-army knife of media players as this plays everything |
| WG Tunnel            | This application allows users to connect to WireGuard VPN tunnels |

Optional recommendations are the following apps:
* JuiceSSH - if you need to SSH into a computer then this works pretty well for it
* Nextcloud apps - if you want to use a Nextcloud server, then these are available in the F-Droid store
* Stealth - an account-free, privacy-oriented, and feature-rich Reddit client
* Termux - if you need command line Linux things, then look into this. I don't use it very often but it's available.
* Monerujo - best phone Monero wallet, but only appears on F-Droid if you add a custom repository ([Instructions here](https://f-droid.monerujo.io/)).
* Fossify File Manager - I really like this FLOSS file manager on F-Droid. I removed it from list above as most users will be fine with the built-in file manager called "Files" :)
* SchildiChat - another Matrix client and fork of the Element client. What this means is that you can run a separate Matrix account alongside the Element client using this one :)
* Tusky - A Mastodon protocol phone app, which is a decentralized social media where no one is in charge!
* Fedilab - Another Mastodon protocol phone app, but this one also has support for Peertube, Pixelfed, Misskey, GNU Social, Pleroma, and Friendica accounts.

If you do actually need something from the G00g's app store, then you can install Aurora Store on F-Droid as a "safe" front-end as you can select to use anon accounts created by this app's dev.

### 4 - Instruction guides

* [How to migrate contacts between phones](https://codeberg.org/jahway603/MapleFone/src/branch/main/guides/contacts-migration.md)
* [How to get started with Element Matrix client](https://codeberg.org/jahway603/matrix-tldr-howto)
* [How to install Applications requiring G00gle Play Services](guides/sandboxplay.md) - If apps like Uber or your banking app are not working, then this will help.

### 5 - Additional Links

* [Banking application compatibility with the GraniteFone](https://privsec.dev/posts/android/banking-applications-compatibility-with-grapheneos/)

### Why you don't want an overpriced "FreedomPhone"

First I'd like to start off with the fact that the XDA Developers Forum is a trusted source for the mobile phone community and has been since 2002. The folks at XDA wrote up [this article](https://www.xda-developers.com/freedom-phone-overpriced-smartphone/) about what exactly the "FreedomPhone" is and why you do not want it. I will give them credit that using "Freedom" in the name is great marketing, but you are getting a product with false claims.

### Future plans

* Create additional user-friendly guides to help people better utilize their new GraniteFone.
* To make a version with Linux running on it, like a [PinePhone](https://www.pine64.org/pinephone/).

### About Me

I have been modifying, customizing, and hacking on Android phones since 2009. Since 2018, I have been exclusively using the [GrapheneOS version of Android](https://grapheneos.org/) and, since it has been such a great experience, I wanted to bring that same experience to others. This document completely shares with you the same phone and applications I use every day.

### Contact Info

If you know me personally, then just ask me about these phones.

If you are online only, then [find me on the Matrix](https://matrix.to/#/@jahway603:meowchat.xyz).

And please NO Signal Messenger because they implemented their [own ishcoin](https://en.wikipedia.org/wiki/MobileCoin) inside their application & the main untrustworthy part of it is that it was done behind everyone's back during 2019 thru 2020, so no more trust for them from me and many other people.

Also, by visiting the [original page about it on Archive.org](https://web.archive.org/web/20201127142851/https://mobilecoin.foundation/about), you will see that they removed this particular statement from their actual website.

```
At MobileCoin, we believe governments have a legitimate interest in regulating the economic lives of their citizens.
```

**I do not agree with this and have stopped using Signal Messenger after learning about this MobileCon.** Everyone is entitled to make their own decisions, but I highly recommend ditching Signal Messenger and to embrace the Matrix or XMPP.

If you want to learn more about how Signal Messenger betrayed all of us, then please [review this video](https://www.bitchute.com/video/tJoO2uWrX1M/). If you want to learn more about why you may want to stop using Signal Messenger, then I recommend [reading the articles linked here](https://codeberg.org/jahway603/yno/src/branch/master/yno-signal.md).

#### License

[GPLv3](LICENSE)
