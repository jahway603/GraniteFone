# How to install Applications requiring G00gle Play Services

Have you found that certain applications are having issues running on your phone? Such as the Uber app or your bank app? Does the application display a pop-up message that it requires G00gle Play Services to run? If the answer is YES to any of these questions, then you have come to correct write-up.

## Background information

Some Android applications require G00gle Play Services be installed on your phone in order for them to work correctly. On a regular store-bought Android phone, G00gle has all permissions enabled with no way of changing that. Your GraniteFone is completely De-Googled, so none of these Google components are installed on it.

You have the Freedom to install G00gle Play Services directly on your GraniteFone, but then that would defeat the purpose of having a De-Googled phone configured to keep G00gle components completely off of it for improved privacy. You do not want to allow G00gle to have that access to your data and this write-up will show you how to configure your phone to safely work with apps like Uber, your bank app, etc.

This document will instruct you on how to install & run the G00gle Play Services on your GraniteFone so that they are "sandboxed", or restricted, from accessing data from your phone or from any of the other apps installed on your phone.

## Setup

Follow these steps and ask if there are any questions:

### 1 - Create a New User Profile

User profiles do not have access to data or applications within other profiles, so we will first instruct you on how to create and setup a New UserPProfile.

1. Open Settings from your Apps.
1. Scroll down and select System.
1. Tap on Multiple Users.
1. Toggle the switch at the top to Enable Multiple Users.
1. Select the Add User option on the screen.
1. Read the pop-up and select Next.
1. Type in a name where it says New User. This name will only be for you to see locally.
1. On the options screen, choose what you would like for your setup. The recommended options are explained below:
    * Allow running in background: We recommend you Enable this option for an app that requires you to stay "logged in" or "online", such as Uber. If your app does not need this, then Disable this.
    * Turn on phone calls / SMS: We recommend you Disable this option, which allows both calls & SMS to be forwarded to the New User Profile from your main profile while actively using the New User Profile. You can always Enable this later if it is needed.
    * App installs and updates: We recommend you set this to "Enabled", which is the default.
    * Install available apps: We recommend you select this and choose the Aurora and F-Droid apps only. This allows these apps to be installed to run in the New User Profile. Take note that the app data between your main profile and the New User Profile will not be shared between the apps - the User Profiles act like two different phones.
    * Delete user: Completely deletes this User Profile with no way to recover it, so be careful!

### 2 - Setup the New User Profile

1. In the New User Profile screen, select "Switch to" whichever name you gave it from above.
1. Select the "Set up now" option.
1. Change the language here from English if required and then select the "Next" option.
1. This next screen requests Location services permission. If the app you want to setup, like Uber, requires a location, then keep this Enabled. If not, then keep this to Disabled. Then select the "Next" option.
1. Select the "Next" option.
1. Setup a PIN or biometric password if you want, but this can be setup later too.
1. Select "Skip" option on the Restore apps & data screen.
1. Select the "Start" option which will result in a vanilla install of GrapheneOS where the New User Profile is completely isolated from your phone's main profile.

The image below shows the process explained within this section:
![](../images/setup-profile_optimized.gif)

### 3 - Install G00gle Play Services in the New User Profile

In this section, we instruct you on how to install the G00gle Play Services, which will be isolated in the New User Profile from the main profile.

1. Navigate into the New User Profile apps and select the app called "Apps". This app is made by GrapheneOS developers.
1. Install "Google Play services" within "Apps", which will install the required components for this to work.
1. Have patience, as this may take some time to finish installing.
1. Once installed, you will want to initialize the G00gle Play Store, which I outline in Step 3a below. **Take note that you DO NOT need to login to a Google account to do this.**

The image below shows the process explained within this section:
![](../images/install-google-play_optimized.gif)

#### 3a - Initialize 

1. Navigate to apps and open the Play Store.
1. Select Sign In, but do not login with any account.
1. Wait for the initialization to complete.
1. Once presented with the login screen, you can now just close it.

#### 4 - Setup Aurora Store to install apps from G00g

If you want to use a gmail account and login to it in the G00gle Play Store, then you can do that, but I do not recommend doing that. Instead, I recommend you use the Aurora Store & below will instruct you on how to set that up to work correctly.

1. Navigate to "Apps".
1. Select "Google Play services".
1. Select "Google Play Store" at the bottom.
1. Selecting Settings on this screen.
1. Select the "Open by default" option.
1. Toggle the "Open supported links" slider to OFF.
1. Now exit that app.
1. Now press and hold your finger on the "Aurora Store" app icon.
1. Select "App info" in the small menu that pops up.
1. Select the "Open by default" option.
1. Toggle the "Open supported links" slider to ON.
1. Select the "Add link" option.
1. Check all three supported links there and click on "Add".
1. Now exit that screen.
1. Now open the Aurora Store app to set it up.
1. Select "Next".
1. Select "Session installer" and then select "Next".
1. Choose whichever Theme you want and then select "Next".
1. Choose your Accent and select "Next".
1. Select Enable on these options and then select "Next".
1. Grant all ther Permissions here and then select "Finish".
1. Now select "Anonymous" option.
1. Final step is to install whichever app you wanted to use via the Aurora Store and it should work.

## Success!

Your phone now has a method to run apps that will not work without G00gle's proprietary crap while isolating the rest of your phone and data from it's evil tentacles.

