# How to use Silence SMS/MMS Application

A lot of people have heard of how solid the end-to-end encryption (E2EE) is the Signal messaging app,
BUT not a lot of people are talking about solutions on how to use this great E2EE tech outside of Signal.

I introduce to you the [Silence Android App](https://silence.im/) which is available on the [F-Droid store]() as well as pre-installed on the GraniteFone as the default texting application. Let's dive in :)

## Setup

1. We have two GraniteFones (or any Android phone) with the Silence app installed on both.
1. We have two users, Alice and Bob.
1. We open the Silence app on both phones.
1. Alice decides to start and opens a new text message in Silence with Bob.
1. Then Alice clicks on the "un-broken lock" icon with an X in it, which is located on the top of the message to Bob on Alice's phone.
1. The Alice clicks on "Start secure session" & answers "Yes".
1. Then both Alice and Bob should see a message on both of their phone messages saying "Key exchange message".
1. Now both Alice and Bob should test sending some messages between their GraniteFones. 
1. Both Alice and Bob should see that they are now sending messages to each other with a little lock icon on them indicating that they are indeed encrypted.

## Success!

Now both Alice and Bob can send SMS/MMS messages between their GraniteFones using Signal's great E2EE technology:

* Without supplying any centralized Signal server any data, as this connection is only between the two phones
* Without submitting their phone number to Signal or any company as it is directly between the two users
* Without submitting their complete list of phone contacts to the Signal company or any company

## To-Do

* Have some screenshots if the above write-up is not helpful for users.

### Credits

jahway603
